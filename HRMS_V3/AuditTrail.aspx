﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AuditTrail.aspx.cs" Inherits="HRMS_V3.AuditTrail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
     <h1 style="font-family: calibri; font-size: large; background-color: #CCCCFF; color: #000066; width: 1310px;">Audit Trail</h1>
<body>
    <form id="form1" runat="server">
    <div style="width: 1309px; background-color: #000066; color: #FFFFFF;">
    
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; <strong>&nbsp;&nbsp; Date </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;&nbsp;&nbsp; Category</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    
        <asp:DropDownList ID="DDLCat" runat="server" DataSourceID ="SqlCateg" DatavalueField ="Category" AutopostBack ="True" AppendDataBoundItems ="True" Height="19px" Width="20px" DataTextField="Category"   >
            <asp:ListItem text ="" Value="%"></asp:ListItem>
        </asp:DropDownList>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>SubCategory</strong>&nbsp;&nbsp;&nbsp;&nbsp;
         <asp:DropDownList ID="DDLSubcat" runat="server" DataSourceID ="SqlSubcat" DatavalueField ="SubCategory" AutopostBack ="True" AppendDataBoundItems ="True" Height="19px" Width="20px" DataTextField="SubCategory"   >
            <asp:ListItem text ="" Value="%"></asp:ListItem>
        </asp:DropDownList>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>Subject </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <asp:DropDownList ID="DDLSub" runat="server" DataSourceID ="SqlSub" DatavalueField ="Subject" AutopostBack ="True" AppendDataBoundItems ="True" Height="19px" Width="20px" DataTextField="Subject"   >
            <asp:ListItem text ="" Value="%"></asp:ListItem>
        </asp:DropDownList>
     
      
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>Change Request </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;<strong>Requester By: </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;Modified By: </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; <strong>Date And Time</strong><br />
        <asp:GridView ID="GvDetails"   runat="server" ShowHeader="False" AllowPaging="True" AutoSorting="True" BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataSourceID="SqlData"  Width="1308px" AutoGenerateColumns="False" style="margin-right: 56px" AllowSorting="True" PageSize ="30">
           
             <Columns>
                <asp:BoundField itemstyle-horizontalalign="Center" DataField="Date" HeaderText="Date" SortExpression="Date" ItemStyle-Width="10%" DataFormatString="{0:MMMM-dd-yyyy}" >
<ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField itemstyle-horizontalalign="Center" DataField="Category" HeaderText="Category" SortExpression="Category" ItemStyle-Width="10%" ConvertEmptyStringToNull="False">
<ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField itemstyle-horizontalalign="Center" DataField="SubCategory" HeaderText="SubCategory" SortExpression="SubCategory"  ItemStyle-Width="10%" ConvertEmptyStringToNull="False">
<ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField itemstyle-horizontalalign="Center" DataField="Subject" HeaderText="Subject" SortExpression="Subject" ItemStyle-Width="10%">
<ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField itemstyle-horizontalalign="Center" DataField="ChangeRequest" HeaderText="ChangeRequest" SortExpression="ChangeRequest" ItemStyle-Width="10%" NullDisplayText="--">
<ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField itemstyle-horizontalalign="Center" DataField="RequestedBy" HeaderText="RequestedBy" SortExpression="RequestedBy" ItemStyle-Width="10%" NullDisplayText="--">
<ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField itemstyle-horizontalalign="Center" DataField="ModifiedBy" HeaderText="Modified By" SortExpression="Modified By" ItemStyle-Width="10%" NullDisplayText="--">
<ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField itemstyle-horizontalalign="Center" DataField="DateAndTime" HeaderText="DateAndTime" SortExpression="DateAndTime" DataFormatString="{0:G}" ItemStyle-Width="15%">
<ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
            </Columns>
            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
            <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
            <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
            <RowStyle BackColor="White" ForeColor="#003399" />
            <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
            <SortedAscendingCellStyle BackColor="#EDF6F6" />
            <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
            <SortedDescendingCellStyle BackColor="#D6DFDF" />
            <SortedDescendingHeaderStyle BackColor="#002876" />


        </asp:GridView>

      
        </div>
        <asp:SqlDataSource ID="SqlCateg" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT DISTINCT [Category] FROM [Audit]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlSubcat" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT DISTINCT [SubCategory] FROM [Audit]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlSub" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT DISTINCT [Subject] FROM [Audit]"></asp:SqlDataSource>
        <br />
        <asp:SqlDataSource ID="SqlData" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT * FROM [Audit]" FilterExpression= "Category LIKE '{0}' AND SubCategory LIKE '{1}' AND Subject LIKE '{2}' ">
            <FilterParameters>
               
              
                <asp:ControlParameter  ControlID="DDLCat"  Name="Category" PropertyName="SelectedValue" Type="String" />
                <asp:ControlParameter ControlID="DDLSubcat"  Name="SubCategory" PropertyName="SelectedValue" Type="String" />
                <asp:ControlParameter ControlID="DDLSub" Name="Subject" PropertyName="SelectedValue" Type="String" />
                  
            </FilterParameters>
        </asp:SqlDataSource>

      
    </form>

</body>
</html>
