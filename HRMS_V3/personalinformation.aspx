﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="personalinformation.aspx.cs" Inherits="HRMS_V3.personalinformation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
 
    <script type = "text/javascript">
    function Confirm() {
        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        if (confirm("Do you want to save data?")) {
            confirm_value.value = "Yes";
        } else {
            confirm_value.value = "No";
        }
        document.forms[0].appendChild(confirm_value);
    }
</script>

    <style type="text/css">
        .auto-style1 {
            height: 26px;
        }
        .auto-style3 {
            height: 26px;
            width: 127px;
        }
        .auto-style10 {
            width: 148px;
        }
        .auto-style11 {
            width: 242px;
        }
        .auto-style12 {
            height: 26px;
            width: 242px;
        }
        .auto-style13 {
            height: 26px;
            width: 148px;
        }
        </style>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $("[id$=datetxt]").datepicker({
                dateFormat: 'MM d, yy',
                minDate: new Date(1960, 1, 1),
                yearRange: "c-40:c",
                showAnim: 'fadeIn',
                changeMonth: true,
                changeYear: true,
                showOn: 'button',
                buttonImageOnly: true,
                buttonImage: 'http://jqueryui.com/resources/demos/datepicker/images/calendar.gif'
               
            });
        });
            </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div class="x_content">

                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                        </thead>

                        <tbody>
                          <tr class="even pointer">
                            <td class="auto-style1" colspan="2" style="background-color: #000080; color: #FFFFFF">Personal information</td>
                            <td colspan="2" class="auto-style1" style="background-color: #000080; color: #FFFFFF">Personal organization</td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style3" style="background-color: #0000FF; color: #FFFFFF">House number</td>
                            <td class="auto-style1">
                                <asp:TextBox ID="housenumtxt" runat="server" Width="258px"></asp:TextBox>
                              </td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style3" style="background-color: #0000FF; color: #FFFFFF">Street name</td>
                            <td class="auto-style1">
                                <asp:TextBox ID="strttxt" runat="server" Width="258px"></asp:TextBox>
                              </td>
                            <td class="auto-style13" style="background-color: #0000FF; color: #FFFFFF">
                                Name of Organization</td>
                            <td class="auto-style12">
                                <asp:TextBox ID="nameoftxt" runat="server" Width="258px"></asp:TextBox>
                              </td>
                          </tr>
                          <tr class="even pointer">
                            <td class="auto-style3" id="barangayOptional" style="background-color: #0000FF; color: #FFFFFF">Barangay</td>
                            <td class=" " id="barangayal">
                                <asp:TextBox ID="brgytxt" runat="server" Width="258px"></asp:TextBox>
                              </td>
                            <td class="auto-style10" id="brangayOptional" style="background-color: #0000FF; color: #FFFFFF">
                                Optional Email</td>
                            <td class="auto-style11" id="bonal">
                                <asp:TextBox ID="optionaltxt" runat="server" Width="258px"></asp:TextBox>
                              </td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style3" style="background-color: #0000FF; color: #FFFFFF">Town</td>
                            <td class=" ">
                                <asp:TextBox ID="towntxt" runat="server" Width="258px"></asp:TextBox>
                              </td>
                            <td class="auto-style10" style="background-color: #0000FF; color: #FFFFFF">
                                Contact No.</td>
                            <td class="auto-style11">
                                <asp:TextBox ID="contacttxt" runat="server" Width="258px"></asp:TextBox>
                              </td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style3" style="background-color: #0000FF; color: #FFFFFF">City</td>
                            <td class=" ">
                                <asp:TextBox ID="citytxt" runat="server" Width="258px"></asp:TextBox>
                              </td>
                            <td class="auto-style10">
                                &nbsp;</td>
                            <td class="auto-style11">
                                &nbsp;</td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style3" style="background-color: #0000FF; color: #FFFFFF">Religion</td>
                            <td class=" ">
                                <asp:TextBox ID="religiontxt" runat="server" Width="258px"></asp:TextBox>
                              </td>
                            <td colspan="2">
                                <asp:Button ID="Button1" runat="server" Text="+ Add Organization" OnClick="Button1_Click" />
                              </td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style3" style="background-color: #0000FF; color: #FFFFFF">Zip Code</td>
                            <td class="auto-style1">
                                <asp:TextBox ID="zipcodetxt" runat="server" Width="255px"></asp:TextBox>
                               </td>
                            <td class="auto-style13">
                                </td>
                            <td class="auto-style12">
                                </td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style3" style="background-color: #0000FF; color: #FFFFFF">Blood Type</td>
                            <td class="auto-style1">
                      
                                <asp:TextBox ID="bloodtypetxt" runat="server" Width="258px"></asp:TextBox>
                              </td>
                            <td class="auto-style13">
                              </td>
                            <td class="auto-style12">
                              </td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style3" style="background-color: #0000FF; color: #FFFFFF">Gender</td>
                            <td class="auto-style1">
                                <asp:TextBox ID="gendertxt" runat="server" Width="258px"></asp:TextBox>
                              </td>
                            <td class="auto-style13">
                                &nbsp;</td>
                            <td class="auto-style12">
                                &nbsp;</td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style3" style="background-color: #0000FF; color: #FFFFFF">Date of Birth</td>
                            <td class="auto-style1">
                                <asp:TextBox ID="datetxt" runat="server" Width="204px"></asp:TextBox>
                              </td>
                            <td class="auto-style13">
                              </td>
                            <td class="auto-style12">
                              </td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style3" style="background-color: #0000FF; color: #FFFFFF">Citezenship</td>
                            <td class="auto-style1">
                                <asp:TextBox ID="citizentxt" runat="server" Width="258px"></asp:TextBox>
                              </td>
                            <td class="auto-style13">
                                &nbsp;</td>
                            <td class="auto-style12">
                                &nbsp;</td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style3"></td>
                            <td class="auto-style1">
                              </td>
                            <td class="auto-style13">
                              </td>
                            <td class="auto-style12">
                              </td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style1" colspan="2" style="color: #FFFFFF; background-color: #0000FF">Personal Contact Information</td>
                            <td class="auto-style13">
                                &nbsp;</td>
                            <td class="auto-style12">
                                &nbsp;</td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style3" style="background-color: #0000FF; color: #FFFFFF">Mobile Number</td>
                            <td class="auto-style1">
                                <asp:TextBox ID="mobiletxt" runat="server" Width="258px"></asp:TextBox>
                              </td>
                            <td class="auto-style13">
                                &nbsp;</td>
                            <td class="auto-style12">
                                &nbsp;</td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style3" style="background-color: #0000FF; color: #FFFFFF">Home Number</td>
                            <td class="auto-style1">
                                <asp:TextBox ID="hometxt" runat="server" Width="258px"></asp:TextBox>
                              </td>
                            <td class="auto-style13">
                                &nbsp;</td>
                            <td class="auto-style12">
                                &nbsp;</td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style3" style="background-color: #0000FF; color: #FFFFFF">Personal Email</td>
                            <td class="auto-style1">
                                <asp:TextBox ID="perosonaltxt" runat="server" Width="258px"></asp:TextBox>
                              </td>
                            <td class="auto-style13">
                                &nbsp;</td>
                            <td class="auto-style12">
                                &nbsp;</td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style3">&nbsp;</td>
                            <td class="auto-style1">
                                &nbsp;</td>
                            <td class="auto-style13">
                                &nbsp;</td>
                            <td class="auto-style12">
                                &nbsp;</td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style1" colspan="2" style="color: #FFFFFF; background-color: #0000FF">Emergency Contact Information</td>
                            <td class="auto-style13">
                                &nbsp;</td>
                            <td class="auto-style12">
                                &nbsp;</td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style3" style="color: #FFFFFF; background-color: #0000FF">Name</td>
                            <td class="auto-style1">
                                <asp:TextBox ID="nametxt" runat="server" Width="257px"></asp:TextBox>
                              </td>
                            <td class="auto-style13">
                                </td>
                            <td class="auto-style12">
                                </td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style3" style="color: #FFFFFF; background-color: #0000FF">Contact Number</td>
                            <td class="auto-style1">
                                <asp:TextBox ID="contactnumtxt" runat="server" Width="254px"></asp:TextBox>
                              </td>
                            <td class="auto-style13">
                                &nbsp;</td>
                            <td class="auto-style12">
                                &nbsp;</td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style3" style="color: #FFFFFF; background-color: #0000FF">Relationship</td>
                            <td class="auto-style1">
                                <asp:TextBox ID="relationtxt" runat="server" Width="251px"></asp:TextBox>
                              </td>
                            <td class="auto-style13">
                                &nbsp;</td>
                            <td class="auto-style12">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                                <asp:LinkButton ID="personalinfo" CausesValidation="false" runat="server" OnClick="personalinfo_Click"> SAVE </asp:LinkButton>
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              </td>
                          </tr>
                          <tr class="odd pointer">
                            <td class="auto-style3">&nbsp;</td>
                            <td class="auto-style1">
                                &nbsp;</td>
                            <td class="auto-style13">
                                &nbsp;</td>
                            <td class="auto-style12">
                                &nbsp;</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
							
    
    </div>
        </div>
    </form>
</body>
</html>

