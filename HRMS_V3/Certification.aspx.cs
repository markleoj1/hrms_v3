﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace HRMS_V3
{
    public partial class Certification : System.Web.UI.Page
    {
        MethodAndVariables crt = new MethodAndVariables();
        protected void Page_Load(object sender, EventArgs e)
        {
            certdate.Text = "";
            adminby.Text = "";
        }

        protected void addcert_Click(object sender, EventArgs e)
        {

            string imgfile = Path.GetFileName(certfile.PostedFile.FileName);
            string imgname = Path.GetFileName(certfile.PostedFile.FileName).Replace(".png", "").Replace(".jpg", "").Replace(".jpeg", "").Replace(".gif", "");
            string imgpth;
            if (imgfile.Contains(".png") == true)
            {
                imgpth = "images/" + imgname + ".png";
            }
            else if (imgfile.Contains(".jpg") == true)
            {
                imgpth = "images/" + imgname + ".jpg";
            }
            else if (imgfile.Contains(".jpeg") == true)
            {
                imgpth = "images/" + imgname + ".jpeg";
            }
            else
            {
                imgpth = "images/" + imgname + "gif";
            }


            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                string cd = Request.Form[certdate.UniqueID];
                string adby = adminby.Text;
                certfile.SaveAs(Server.MapPath("images/" + imgfile));
                crt.AddCrt(cd, adby, imgname, imgpth);
                certdrp.DataBind();
            }
        }

        protected void certdrp_SelectedIndexChanged(object sender, EventArgs e)
        {
            string certname = certdrp.SelectedValue.ToString();
            string certfile;

            certdate.Text = crt.getCertDate(certname);
            adminby.Text = crt.getAdminBy(certname);
            certfile = crt.getCertImg(certname);
            certimg.ImageUrl = certfile;
        }
    }
}