﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;

namespace HRMS_V3.App_Code
{
    public class MethodAndVariables
    {
        public MethodAndVariables()
        {
            ConString = ConfigurationManager.ConnectionStrings["MISConnectionString"].ConnectionString.ToString();
            SysCon = new SqlConnection(ConString);
        }
        public SqlConnection SysCon;
        static string ConString;

        public void AddLeaveRequest(DateTime LeaveDate, bool isHalfday, string LeaveType, string Paid)
        {
            SysCon.Open();
            SqlCommand insertCmd = new SqlCommand("Insert into tblLeaveManagement values(@LeaveDate,@AppliedDate,@isHalfDay,@LeaveType,@Type,@Status,@DateModified,@EmployeeId)", SysCon);
            insertCmd.CommandType = CommandType.Text;
            insertCmd.Parameters.AddWithValue("@LeaveDate", LeaveDate);
            insertCmd.Parameters.AddWithValue("@AppliedDate", DateTime.Now.ToShortDateString());
            insertCmd.Parameters.AddWithValue("@isHalfDay", isHalfday);
            insertCmd.Parameters.AddWithValue("@LeaveType", LeaveType);
            insertCmd.Parameters.AddWithValue("@Type", Paid);
            insertCmd.Parameters.AddWithValue("@Status", "Pending");
            insertCmd.Parameters.AddWithValue("@DateModified", DateTime.Now.ToShortDateString());
            insertCmd.Parameters.AddWithValue("@EmployeeId", 1001);
            insertCmd.ExecuteNonQuery();
            SysCon.Close();
        }

        public void AddTraining(string tdate, string ddate, string fname, string fpath)
        {
            SysCon.Open();
            SqlCommand comd = new SqlCommand("select count(TrainID) from ProfTrain", SysCon);
            int count = Convert.ToInt32(comd.ExecuteScalar());
            count += 1;
            SysCon.Close();

            string query = "insert into ProfTrain(TrainID,Traindate,Duedate,Topicname,Topicfile) values (@trainid, @traindate, @duedate, @topicname, @topicfile)";
            SqlCommand cmd = new SqlCommand(query);
            cmd.Connection = SysCon;
            cmd.Parameters.AddWithValue("@trainid", count);
            cmd.Parameters.AddWithValue("@traindate", tdate);
            cmd.Parameters.AddWithValue("@duedate", ddate);
            cmd.Parameters.AddWithValue("@topicname", fname);
            cmd.Parameters.AddWithValue("@topicfile", fpath);
            SysCon.Open();
            cmd.ExecuteNonQuery();
            SysCon.Close();
        }




        public string getTrainingDate(string fileName)
        {
            string flData;
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("select * from ProfTrain where Topicname='" + fileName + "'", SysCon);
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();

            flData = reader["Traindate"].ToString();

            reader.Close();
            cmd.Dispose();
            SysCon.Close();

            return flData;
        }

        public string getTrainingDueDate(string fileName)
        {
            string flData;
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("select * from ProfTrain where Topicname='" + fileName + "'", SysCon);
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();

            flData = reader["Duedate"].ToString();

            reader.Close();
            cmd.Dispose();
            SysCon.Close();

            return flData;
        }

        public string getTrainingDueTopicFile(string fileName)
        {
            string flData;
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("select * from ProfTrain where Topicname='" + fileName + "'", SysCon);
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();

            flData = reader["Topicfile"].ToString();

            reader.Close();
            cmd.Dispose();
            SysCon.Close();

            return flData;
        }

        public void AddCrt(string cdate, string admnby, string iname, string ipath)
        {
            SysCon.Open();
            SqlCommand comd = new SqlCommand("select count(CertID) from ProfCertification", SysCon);
            int count = Convert.ToInt32(comd.ExecuteScalar());
            count += 1;
            SysCon.Close();

            string query = "insert into ProfCertification(CertID,Certdate,Adminby,Certtopic,Certimg) values (@certid, @certdate, @adminby, @certtopic, @certimg)";
            SqlCommand cmd = new SqlCommand(query);
            cmd.Connection = SysCon;
            cmd.Parameters.AddWithValue("@certid", count);
            cmd.Parameters.AddWithValue("@certdate", cdate);
            cmd.Parameters.AddWithValue("@adminby", admnby);
            cmd.Parameters.AddWithValue("@certtopic", iname);
            cmd.Parameters.AddWithValue("@certimg", ipath);
            SysCon.Open();
            cmd.ExecuteNonQuery();
            SysCon.Close();
        }

        public string getCertDate(string cert)
        {
            string CertDt;
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("select * from ProfCertification where Certtopic='" + cert + "'", SysCon);
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();

            CertDt = reader["CertDate"].ToString();

            reader.Close();
            cmd.Dispose();
            SysCon.Close();

            return CertDt;
        }

        public string getAdminBy(string cert)
        {
            string Adby;
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("select * from ProfCertification where Certtopic='" + cert + "'", SysCon);
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();

            Adby = reader["Adminby"].ToString();

            reader.Close();
            cmd.Dispose();
            SysCon.Close();

            return Adby;
        }

        public string getCertImg(string cert)
        {
            string cimg;
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("select * from ProfCertification where Certtopic='" + cert + "'", SysCon);
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();

            cimg = reader["Certimg"].ToString();

            reader.Close();
            cmd.Dispose();
            SysCon.Close();

            return cimg;
        }


        public void AddAwrd(string adate, string aname, string apath)
        {
            SysCon.Open();
            SqlCommand comd = new SqlCommand("select count(AwardID) from ProfAward", SysCon);
            int count = Convert.ToInt32(comd.ExecuteScalar());
            count += 1;
            SysCon.Close();

            string query = "insert into ProfAward(AwardID,Awardname,Awarddate,Awardimg) values (@awardid, @awardname, @awarddate, @awardimg)";
            SqlCommand cmd = new SqlCommand(query);
            cmd.Connection = SysCon;
            cmd.Parameters.AddWithValue("@awardid", count);
            cmd.Parameters.AddWithValue("@awardname", aname);
            cmd.Parameters.AddWithValue("@awarddate", adate);
            cmd.Parameters.AddWithValue("@awardimg", apath);
            SysCon.Open();
            cmd.ExecuteNonQuery();
            SysCon.Close();
        }

        public string getAwrdDate(string awd)
        {
            string AwdDt;
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("select * from ProfAward where Awardname='" + awd + "'", SysCon);
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();

            AwdDt = reader["Awarddate"].ToString();

            reader.Close();
            cmd.Dispose();
            SysCon.Close();

            return AwdDt;
        }

        public string getAwrdImg(string awd)
        {
            string aimg;
            SysCon.Open();
            SqlCommand cmd = new SqlCommand("select * from ProfAward where Awardname='" + awd + "'", SysCon);
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();

            aimg = reader["Awardimg"].ToString();

            reader.Close();
            cmd.Dispose();
            SysCon.Close();

            return aimg;
        }
    }
}