﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoanProfile.aspx.cs" Inherits="HRMS_V3.LoanProfile" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
     <h1 style="font-family: calibri; font-size: large; background-color: #CCCCFF; width: 920px; color: #000066; height: 39px;">Loan Profile</h1>
<body>
    <form id="form1" runat="server">
        <div>
    
        <br />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataSourceID="SqlDataLoan">
            <Columns>
                <asp:BoundField DataField="Loan" HeaderText="Loan" SortExpression="Loan" ItemStyle-Width="10%">
<ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type" ItemStyle-Width="10%">
<ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="Principal" HeaderText="Principal" SortExpression="Principal" ItemStyle-Width="10%" DataFormatString="{0:N}">
<ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="MonthsToPay" HeaderText="MonthsToPay" SortExpression="MonthsToPay" ItemStyle-Width="10%">
<ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="MonthsRemaining" HeaderText="MonthsRemaining" SortExpression="MonthsRemaining" ItemStyle-Width="10%">
<ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="TotalPaid" HeaderText="TotalPaid" SortExpression="TotalPaid" ItemStyle-Width="10%" DataFormatString="{0:N}">
<ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="ModifiedBy" HeaderText="ModifiedBy" SortExpression="ModifiedBy" ItemStyle-Width="10%">
<ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="DateAndTime" HeaderText="DateAndTime" SortExpression="DateAndTime" ItemStyle-Width="18%">
<ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
            </Columns>
            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
            <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
            <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
            <RowStyle BackColor="White" ForeColor="#003399" />
            <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
            <SortedAscendingCellStyle BackColor="#EDF6F6" />
            <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
            <SortedDescendingCellStyle BackColor="#D6DFDF" />
            <SortedDescendingHeaderStyle BackColor="#002876" />
        </asp:GridView>
            <asp:SqlDataSource ID="SqlDataLoan" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT * FROM [LoanProfile]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT * FROM [LoanProfile]"></asp:SqlDataSource>
    
    </div>
    </form>
</body>
</html>