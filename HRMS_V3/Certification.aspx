﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Certification.aspx.cs" Inherits="HRMS_V3.Certification" %>

<!DOCTYPE html>
<script runat="server">

    
</script>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

<script type="text/javascript">
 
    $(function () {
        $("[id$=certdate]").datepicker({
            dateFormat: 'MM d, yy',
            maxDate: '+0m +0w',
            showAnim: 'fadeIn',
            changeMonth: true,
            changeYear: true,
            showOn: 'button',
            buttonImageOnly: true,
            buttonImage: 'http://jqueryui.com/resources/demos/datepicker/images/calendar.gif'
        });
    });

    function Confirm() {
        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        if (confirm("Do you want to save data?")) {
            confirm_value.value = "Yes";
        }
        document.forms[0].appendChild(confirm_value);
    }
</script>
    <style type="text/css">

        .auto-style1 {
            width: 124px;
        }
        .auto-style3 {
            width: 124px;
            height: 26px;
        }
        .auto-style4 {
            height: 26px;
        }
        .auto-style5 {
            width: 98%;
            margin-right: 0px;
        }
        .auto-style6 {
            float: left;
            width: 39%;
            height: 183px;
        }
    </style>
</head>
<body>
    <form id="form2" runat="server">
    <div id="main" style="height: 100%;width:100%">
    <div id="leftclm" class="auto-style6">

        <asp:Label ID="Label1" runat="server" BackColor="Blue" ForeColor="White" Text="Certification" Width="100%"></asp:Label>
        <br />

        <table id="Traintbl"style="border:solid gray 1px" class="auto-style5">
            <tr>
                <td class="auto-style3">
                    <asp:Label ID="certdatelbl" runat="server" BackColor="#0066FF" ForeColor="White" Text="Certification Date" Width="100%"></asp:Label>
                </td>
                <td class="auto-style4">
                     <asp:TextBox ID="certdate" runat="server" Width="216px"></asp:TextBox>
                </td>
              
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="adminbylbl" runat="server" BackColor="#0066FF" ForeColor="White" Text="Administered by" Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="adminby" runat="server" Width="214px"></asp:TextBox>
                </td>
              
            </tr>
            <tr>
                <td class="auto-style3">
                    <asp:Label ID="Topiclbl" runat="server" BackColor="#0066FF" ForeColor="White" Text="Topic" Width="100%"></asp:Label>
                </td>
                <td class="auto-style4">
                    <asp:DropDownList ID="certdrp" runat="server"  AutoPostBack="True" Width="214px" DataSourceID="certdata" DataTextField="Certtopic" DataValueField="Certtopic" OnSelectedIndexChanged="certdrp_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="certdata" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT [Certtopic] FROM [ProfCertification]"></asp:SqlDataSource>
                </td>
               
            </tr>
        </table>
        <asp:FileUpload ID="certfile" runat="server" accept=".png,.jpg,.jpeg,.gif" />
        <br />
        <asp:Button ID="addcert" runat="server" Text="+ Add Certification" OnClick="addcert_Click" OnClientClick = "Confirm()"/>
        <br />
        <br />
    </div>
    <div id="rightclm" style="height:auto;width:50%;float:left">
        <asp:Image ID="certimg" runat="server" Height="320px" Width="571px" />
    </div>
    </div>
    </form>
    </body>
</html>
