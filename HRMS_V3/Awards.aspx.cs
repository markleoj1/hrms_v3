﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
namespace HRMS_V3
{
    public partial class Awards : System.Web.UI.Page
    {
        MethodAndVariables awrd = new MethodAndVariables();
        protected void Page_Load(object sender, EventArgs e)
        {
            awarddate.Text = "";
        }


        protected void ddlawards_SelectedIndexChanged(object sender, EventArgs e)
        {
            string awdname = ddlawards.SelectedValue.ToString();
            string awrdfile;

            awarddate.Text = awrd.getAwrdDate(awdname);
            awrdfile = awrd.getAwrdImg(awdname);
            awardimg.ImageUrl = awrdfile;
        }

        protected void awrdbtn_Click(object sender, EventArgs e)
        {



            string imgfile = Path.GetFileName(awardfile.PostedFile.FileName);
            string imgname = Path.GetFileName(awardfile.PostedFile.FileName).Replace(".png", "").Replace(".jpg", "").Replace(".jpeg", "").Replace(".gif", "");
            string imgpth;
            if (imgfile.Contains(".png") == true)
            {
                imgpth = "images/" + imgname + ".png";
            }
            else if (imgfile.Contains(".jpg") == true)
            {
                imgpth = "images/" + imgname + ".jpg";
            }
            else if (imgfile.Contains(".jpeg") == true)
            {
                imgpth = "images/" + imgname + ".jpeg";
            }
            else
            {
                imgpth = "images/" + imgname + "gif";
            }


            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                string ad = Request.Form[awarddate.UniqueID];
                awardfile.SaveAs(Server.MapPath("images/" + imgfile));
                awrd.AddAwrd(ad, imgname, imgpth);
                ddlawards.DataBind();
            }

        }
    }
}