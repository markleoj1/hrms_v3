﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Examination.aspx.cs" Inherits="HRMS_V3.Examination" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

<script type="text/javascript">
 
    $(function () {
        $("[id$=subdate]").datepicker({
            dateFormat: 'MM d, yy',
            yearRange: "c-40:c",
            showAnim: 'fadeIn',
            changeMonth: true,
            changeYear: true,
            showOn: 'button',
            buttonImageOnly: true,
            buttonImage: 'http://jqueryui.com/resources/demos/datepicker/images/calendar.gif'
        });
    });
</script>
    <style type="text/css">
        .auto-style1 {
            width: 124px;
        }
    </style>
</head>
<body>
    <form id="form2" runat="server">
    <div id="main" style="height: 100%;width:100%">
    <div id="leftclm" style="height:auto;width:48%;float:left">

        <asp:Label ID="Label1" runat="server" BackColor="Blue" ForeColor="White" Text="Examinations" Width="100%"></asp:Label>
        <br />

        <table id="Traintbl"style="width:100%;border:solid gray 1px">
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="subjectlbl" runat="server" BackColor="#0066FF" ForeColor="White" Text="Subject" Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="subejctdrp" runat="server" Height="31px" Width="299px">
                    </asp:DropDownList>
                </td>
              
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="datelbl" runat="server" BackColor="#0066FF" ForeColor="White" Text="Date" Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="subdate" runat="server" ReadOnly="true" Width="288px"></asp:TextBox>
                </td>
              
            </tr>
          
        </table>
        <asp:FileUpload ID="subjectfile" runat="server" Height="26px" Width="212px" />
        <br />
        <asp:Button ID="addexambtn" runat="server" Text="+ Add Examination" OnClick="Button1_Click" />
        <br />
    </div>
    <div id="rightclm" style="height:auto;width:50%;float:right">
        <iframe id="I1" name="I1" style="height:610px; width:90%"></iframe>
    </div>
    </div>
    </form>
    </body>
</html>
