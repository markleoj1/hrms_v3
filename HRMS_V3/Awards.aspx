﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Awards.aspx.cs" Inherits="HRMS_V3.Awards" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

<script type="text/javascript">
 
    $(function () {
        $("[id$=awarddate]").datepicker({
            dateFormat: 'MM d, yy',
            maxDate: '+0m +0w',
            showAnim: 'fadeIn',
            changeMonth: true,
            changeYear: true,
            showOn: 'button',
            buttonImageOnly: true,
            buttonImage: 'http://jqueryui.com/resources/demos/datepicker/images/calendar.gif'
        });
    });

    function Confirm() {
        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        if (confirm("Do you want to save data?")) {
            confirm_value.value = "Yes";
        }
        document.forms[0].appendChild(confirm_value);
    }
</script>
    <style type="text/css">

        .auto-style1 {
            width: 124px;
        }
    </style>
</head>
<body>
    <form id="form2" runat="server">
    <div id="main" style="height: 100%;width:100%">
    <div id="leftclm" style="height:auto;width:48%;float:left">

        <asp:Label ID="Label1" runat="server" BackColor="Blue" ForeColor="White" Text="Awards and Recognition" Width="100%"></asp:Label>
        <br />

        <table id="Traintbl"style="width:100%;border:solid gray 1px">
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="awardlbl" runat="server" BackColor="#0066FF" ForeColor="White" Text="Award" Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlawards" runat="server" Width="169px" AutoPostBack="True" DataSourceID="awarddt" DataTextField="Awardname" DataValueField="Awardname" OnSelectedIndexChanged="ddlawards_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="awarddt" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT [Awardname] FROM [ProfAward]"></asp:SqlDataSource>
                </td>
              
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="datelbl" runat="server" BackColor="#0066FF" ForeColor="White" Text="Date" Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="awarddate" runat="server" ReadOnly = "false"></asp:TextBox>
                </td>
              
            </tr>
          
        </table>
        <asp:FileUpload ID="awardfile" runat="server" accept=".png,.jpg,.jpeg,.gif" />
        <br />
        <asp:Button ID="awrdbtn" runat="server" Text="+ Add Awards / Recognition" Width="195px" OnClick="awrdbtn_Click" OnClientClick = "Confirm()"/>
        <br />
        <br />
    </div>
    <div id="rightclm" style="height:auto;width:50%;float:right">
        <asp:Image ID="awardimg" runat="server" Height="50%" Width="100%" />
    </div>
    </div>
    </form>
    </body>
</html>
