﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Training.aspx.cs" Inherits="HRMS_V3.Training" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title></title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

<script type="text/javascript">
 
    $(function () {
        $("[id$=traindate]").datepicker({
            dateFormat: 'MM d, yy',
            minDate: new Date(1960,1,1),
            maxDate: '+0m +0w',
            showAnim: 'fadeIn',
            changeMonth: true,
            changeYear: true,
            showOn: 'button',
            buttonImageOnly: true,
            buttonImage: 'http://jqueryui.com/resources/demos/datepicker/images/calendar.gif',
            onSelect: function (dateText, inst) {
                $("[id$=duedate]").datepicker("option", "minDate",
                $("[id$=traindate]").datepicker("getDate"));
            }
        });
        $("#txtEndDate").datepicker();
    });


    $(function () {
        $("[id$=duedate]").datepicker({
            dateFormat: 'MM d, yy',
            maxDate: '+0m +0w',
            showAnim: 'fadeIn',
            changeMonth: true,
            changeYear: true,
            showOn: 'button',
            buttonImageOnly: true,
            buttonImage: 'http://jqueryui.com/resources/demos/datepicker/images/calendar.gif'
        });
    });

    function Confirm() {
        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        if (confirm("Do you want to save data?")) {
            confirm_value.value = "Yes";
        } 
        document.forms[0].appendChild(confirm_value);
    }


</script>

    <style type="text/css">
        .auto-style1 {
            width: 104px;
        }
        .auto-style2 {
            width: 104px;
            height: 30px;
        }
        .auto-style3 {
            height: 30px;
        }      

        .auto-style4 {
            margin-left: 0px;
        }

        .auto-style5 {
            height: auto;
            width: 27%;
            float: left;
        }
        
        </style>
</head>
<body style="height: 100%;width:100%">
   
    <form id="form1" runat="server">


        <div id="main" style="height: 100%;width:100%">
            <div id="leftclm" class="auto-style5">
                 <asp:Label ID="Label1" runat="server" BackColor="Blue" ForeColor="White" Text="TRAINING" Width="100%"></asp:Label>
                    <br />
        <table id="Traintbl"style="width:100%;border:solid gray 1px">
            <tr>
                <td class="auto-style2">
                    <p id="trndate">Train Date</p></td>
                <td class="auto-style3">

                            <asp:TextBox ID="traindate" runat="server" Width="175px" ></asp:TextBox> 
                      
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <p id="dudate">Due Date</p></td>
                <td>

                            <asp:TextBox ID="duedate" runat="server" Width="175px"></asp:TextBox> 

                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <p id="topic">Topicc</p></td>
                <td class="auto-style3">

                            <asp:DropDownList ID="topicdrp" runat="server" AutoPostBack="True" Height="16px" Width="234px" DataSourceID="traindata" DataTextField="Topicname" OnSelectedIndexChanged="topicdrp_SelectedIndexChanged">
                      </asp:DropDownList>
                               <asp:SqlDataSource ID="traindata" runat="server" ConnectionString="Data Source=ILLIMITADO01\SQLEXPRESS;Initial Catalog=sample_db;User ID=sa;Password=securedmgt@1230" SelectCommand="SELECT * FROM [ProfTrain]" ProviderName="System.Data.SqlClient"></asp:SqlDataSource>

                      
                     
                    </td>
               
            </tr>
        </table>
                <asp:FileUpload ID="topicfile" runat="server" accept="application/pdf" Height="26px" Width="212px"/>
               
       
        <br />
       
                 <asp:Button ID="addbtn" runat="server" OnClick="uploadbtn_Click" OnClientClick = "Confirm()" Text="+ Add Training" Width="119px" CssClass="auto-style4" />
       
        <br />
        <br />
    </div>
    <div id="rightclm" style="float:left;width:65%">
                 <iframe id="trainview" src="" runat="server" name="trainview" style="height:550px; width:65%"></iframe>
        </div>
    </div>
    </form>
          
    </body>
</html>