﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeBehind="index.aspx.cs" Inherits="HRMS_V3.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<head runat="server">
    <title>HRMS</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
</head>
    <style>
        
        .RED {
            background-color:red;
        }
        
        .BLUE {
            background-color: blue;
        }
        
        .GREEN {
            background-color: green;
        }
        
        .YELLOW {
            background-color: yellow;
        }
        
        .no-gutters{
            margin: 0;
            padding: 0;
        }
        
        /* Side Navigation START*/
        
        .side_nav {
    height: 100%;
    width: 80px;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #003365;
    overflow-y: hidden;
    transition: 0.5s;
    padding-top: 100px;
    color: white;
            
       
}
        
        .side_nav:hover {
    height: 100%;
    width: 210px;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #003365;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 20px;  
        }
          
        #profile_pic {
            height: 120px;
            width: 120px;
            border-radius: 100%;
            position: relative;
            left: 45px;
            top: -300px;
            transition: .6s ease-out;
            
            padding: 0;
            margin: 0;
            
            border: 0;
            cursor: pointer;
            
            overflow: hidden;
            
            align-content: center;
            
            
        }
        
        #profile_pic img{
	      
            height: 100%;
            background-color: black;
            
        }
        
        .side_nav:hover #profile_pic {
            position: relative;
            border: 3px solid white;
            top: 30px;
        }

        .profile_name{
            position: relative;
            bottom: 150px;
            text-align: center;
            width: 100%;
            padding: 0px 3px 0px 3px;
            margin: 0;
            transition: bottom .3s ease-in;
           
            
        }
        
        .side_nav:hover .profile_name{
            position: relative;
            bottom: -35px;
            
            
        }
        
        .side_menu{ 
            position: relative;
            top: -130px;
            width: 80px;
            transition: .5s;
            text-align: left;
           
            transition: .3s ease-in; 
        }
        
        .side_nav:hover .side_menu {
	       position: relative;
            top:50px;
            width: 100%;
        }
        
        .side_menu ul {
            
            list-style: none;
            padding: 0px;
            margin: 0;
            transition: .5s;      
        }
        
        .side_menu ul > li {
            height: 46px;
            padding-top: 5px;
        }
          
        .side_menu_label {
            font-size: 10px;
            font-family: sans-serif;
            transition: .3s;
            padding: 0px;
            margin: 0px;
            text-align: center;  
        }
        
        .side_nav:hover .side_menu > ul .side_menu_label {
            position:  relative;
            bottom: 26px;
            font-size: 16px;
            text-align: left;
            padding-left: 60px; 
        }
        
        .side_menu a {
            text-decoration: none;
            color:white;
            padding: 0;
            margin: 0;
            transition: .5s;   
        }
        
        .side_menu_option {  
            margin-bottom: 5px;
            transition: .3s;
        }
        
        .side_nav:hover ul > .side_menu_option {
	       height: 30px;
            padding-top: 4px;
        }
        
        .side_menu_icon {
            
            font-size: 24px;
            transition: .3s;
            padding-left: 30px;
            
        }
        
        .side_nav:hover .side_menu > ul .side_menu_icon {
            padding-right: 165px;
            font-size: 26px;
            
        }
        
        .side_nav_submenu li {
            text-align: left;
           padding-left: 30px;
            padding-top: 2px !important;
            overflow: hidden;
            margin-bottom: 2px;
            height: 24px !important;
            width: inherit;
            overflow: hidden;
            background-color: #002345;
        }
        
        .side_menu_option:hover {
            background-color: #002950;
        }
        
        /* Side Navigation END*/
        
        /* ///////////////////////////////////////DIVIDER//////////////////////////////////////////////////// */
        
        /* Top Navigation START */
        
        .top_nav {
            background-color: #ccd6e0;
            margin-left: 80px;
            height: 40px;
            padding-left: 15px;
            padding-right: 10px;
            padding-top: 2px;
            overflow: hidden;  
            transition: .5s;
            z-index: 0;
            
        }
        
        .side_nav:hover .top_nav {
            margin-left: 210px;
            transition: .5s ease-out;
                
        }
        
        .top_nav:hover {
            height: 60px;
        }
        
        .top_nav_title {
            width: 50%;
            color: #003365;
           
        }
        
        .top_menu_container {
            float: right;
            padding-top: 0px;
            margin: 0;
        }
        
        
        
        .top_menu_option{
            
            padding: 0px;
            margin: 0;    
           
        }
        
        
        
        .top_menu_option li {
            list-style: none;
            padding: 3px 15px 2px 15px;
            float:left;
            text-align: center;
            cursor: pointer;
            transition: .5s;
            
        }
        
        
        
        .top_menu_option_active:hover {
            border-top: 3px solid #0063c4; 
            border-bottom: 3px solid #0063c4;
           
        }
        
        .top_menu_option a {
            text-decoration: none;
            color: #003365;
            transition: .2s;
        }
        
        .top_menu_option a:hover {
            color: #0063c4;
        }
        
        .top_menu_label {
            position: relative;
            top: 5px;
        }
        
        

        
        /* Top Navigation EMD*/
        
         /* ///////////////////////////////////////DIVIDER//////////////////////////////////////////////////// */
        
        /* main START*/
 
        #main {
            background-color: aqua;
            margin-left: 80px;
            margin-top: 40px;
            transition: .5s;
            height: 100%;
        }
        
        #main:hover {
            background-color: red;
            margin-left: 80px;
        }
        
        body:hover > #main {
            background-color: yellow;
        }
         /* main END */
        

    </style>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="nav_bars">
        
        <!--side_nav START-->    
        <div class="side_nav">
            
            <div id="profile_pic">
                <img src="images/BenchWorX%20Logo_NO%20BG_Artboard%204.png">
            </div>
            
           <div class="profile_name">
                <h5>Steven Bitac</h5>
                    
            </div>
    
            <div class="side_menu">
                
                <ul>
                    
                    <!--TimeKeeping START-->
                    <li class="side_menu_option">
                        <a href="#">
                            <i class="fa fa-clock-o fa-3x side_menu_icon">  </i>
                            <p class="side_menu_label">Time&nbsp;Keeping</p>
                        </a>      
                    </li>
                    

                    <!--TimeKeeping END-->
                    
                    
                    <!--Organize Leave START-->
                    <li class="side_menu_option">
                        <a href="#">
                            <i class="fa fa-plane fa-3x side_menu_icon">  </i>
                            <p class="side_menu_label">Organize&nbsp;Leave</p>
                        </a>      
                    </li>
                    
                    <ul class="collapse side_nav_submenu" id="Organize_Sub">
                        <li>EGSS</li>
                        <li>EGSS REALLY LONG NAME</li>
                        <li>EGSS</li>
                        <li>EGSS</li>
                    </ul>
                    <!--Organize Leave END-->
                    
                    <!--Profile START-->
                    <li class="side_menu_option" data-toggle="collapse" data-target="#Profile_Sub">
                        <a href="#">
                            <i class="fa fa-user fa-3x side_menu_icon">  </i>
                            <p class="side_menu_label">Profile</p>
                        </a>      
                    </li>
                    
                    <ul class="collapse side_nav_submenu" id="Profile_Sub">
                        <li><a href="#">Personnel information</a></li>
                        <li>Skillset</li>
                        <li>Financial Statement</li>
                        <li>Audit Trail</li>
                    </ul>
                    <!--Profile END-->
                    
                    <!--Master Control START-->
                    <li class="side_menu_option" data-toggle="collapse" data-target="#Master_Sub">
                        <a href="#">
                            <i class="fa fa-key fa-3x side_menu_icon">  </i>
                            <p class="side_menu_label">Master&nbsp;Control</p>
                        </a>      
                    </li>
                    
                    <ul class="collapse side_nav_submenu" id="Master_Sub">
                        <li>Profile</li>
                        <li>Timekeeping</li>
                        <li>System</li>
                        <li>Benefits</li>
                    </ul>
                    <!--Master Leave END-->
                    
                            
                </ul><!-- main ul END-->
                
            </div><!--side_menumenu END-->         
   
            <div class="top_nav navbar-fixed-top">
                
                <div class="top_menu_container">
                    <ul class="top_menu_option" >
                        <li class="top_menu_option_active"> <a href="#"> Timer </a></li>
                        <li class="top_menu_option_active"> <a href="#"> <i class="fa fa-hourglass-start fa-2x"></i><br> <span class="top_menu_label">IN</span> </a> </li>
                        <li class="top_menu_option_active"> <a href="#"> <i class="fa fa-coffee fa-2x"></i><br><span class="top_menu_label">BREAK</span></a> </li>
                        <li class="top_menu_option_active"> <a href="#"> <i class="fa fa-hourglass-end fa-2x"></i><br> <span class="top_menu_label">OUT</span> </a> </li>
                        <li class="top_menu_option_active"> <a href="#"> <i class="fa fa-sign-out fa-2x"></i><br> <span class="top_menu_label">Log-Out</span> </a> </li>   
                    </ul>
                </div>
                
                <div class="top_nav_title">
                    <h4>PROFILE</h4>
                </div>
                
            </div> <!--top_nav END-->
            
            
        </div>
        <!-- side_nav END-->
            
     </div> 
            
            <div id="main">
                MAIN BODY
                
            </div>
            
                 
    </body>
    </div>
    </form>

</body>
    <script>
        
      
        
    </script>
    
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
</html>
