﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SalaryInformation.aspx.cs" Inherits="HRMS_V3.SalaryInformation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">

        .auto-style1 {
            height: 23px;
        }
        .table {
            width: 509px;
        }
        .auto-style2 {
            height: 23px;
            font-weight: bold;
            width: 329px;
        }
        .table {
            border-collapse: separate; border-spacing: 5px;  /* cellspacing="5" */
 border-collapse: collapse; border-spacing: 0;
            height: 477px;
        }
        .auto-style3 {
            height: 23px;
            width: 329px;
        }
        .auto-style4 {
            width: 329px;
        }
    </style>
    <script>

    </script>
</head>
    <h1 style="font-family: calibri; font-size: large; background-color: #CCCCFF; color: #000066; width: 804px;">Salary Information</h1>
    

<body>
    <form id="form1" runat="server">
    <div>
        <div style="float:left;width:47%;" >
    
            <table class="table table-striped table-condensed">
                  <tr>
                      <th class="auto-style3" style="background-color: #000066; font-family: calibri; color: #FFFFFF;">Description</th>
                      <th class="auto-style1" style="background-color: #000066; font-family: calibri; color: #FFFFFF;">Date</th>
                  </tr>
                  <tr>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;" class="auto-style3">Hourly</td>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;"class="auto-style1" colspan="1">
                        <asp:Label ID="Hourly" runat="server"></asp:Label>
                      </td>
                  </tr>
                  <tr>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;" class="auto-style4">Daily</td>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;"">
                        <asp:Label ID="Daily" runat="server"></asp:Label>
                      </td>
                  </tr>
                  <tr>
                    <td class="auto-style3" style="border-style: solid; border-width: thin; font-family: calibri;">Monthly</td>
                    <td class="auto-style1" style="border-style: solid; border-width: thin; font-family: calibri;">
                        <asp:Label ID="Monthly" runat="server"></asp:Label>
                      </td>
                  </tr>
                  <tr>
                    <td style="background-color: #000066; font-family: calibri; color: #FFFFFF;" class="auto-style4"><strong>Taxable Allowance</strong></td>
                    <td style="background-color: #000066; font-family: calibri; color: #FFFFFF;">&nbsp;</td>
                    </tr>
                  <tr>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;" class="auto-style3">Complexity Premium</td>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;" class="auto-style1">
                        <asp:Label ID="ComplexityPremium" runat="server"></asp:Label>
                      </td>
                    </tr>
                  <tr>
                    <td class="auto-style2" style="background-color: #000066; font-family: calibri; color: #FFFFFF;">Non Taxable Allowance</td>
                    <td class="auto-style1" style="background-color: #000066; font-family: calibri; color: #FFFFFF;"></td>
                    </tr>
                  <tr>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;" class="auto-style4">Food</td>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;">
                        <asp:Label ID="Food" runat="server"></asp:Label>
                      </td>
                    </tr>
                  <tr>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;" class="auto-style4">Travel</td>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;">
                        <asp:Label ID="Travel" runat="server"></asp:Label>
                      </td>
                  </tr>
                  <tr>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;" class="auto-style3">Clothing</td>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;" class="auto-style1">
                        <asp:Label ID="Clothing" runat="server"></asp:Label>
                      </td>
                  </tr>
                <tr>
                        <td style="border-style: solid; border-width: thin; font-family: calibri;" class="auto-style4"><b>Night Differential</b></td>
                    
                    <td class="auto-style1" style="border-style: solid; border-width: thin; font-family: calibri;">
                        <asp:Label ID="NightDifferential" runat="server"></asp:Label>
                    </td>
                </tr>
                  <tr>
                        <td style="border-style: solid; border-width: thin; font-family: calibri;" class="auto-style3"><b>Regular Overtime %</b></td>
                      
                    <td style="border-style: solid; border-width: thin; font-family: calibri;" class="auto-style1">
                        <asp:Label ID="RegularOvertime" runat="server"></asp:Label>
                      </td>
                    </tr>
                  <tr>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;" class="auto-style4"><b>Regular Holiday %</b></td>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;">
                        <asp:Label ID="RegularHoliday" runat="server"></asp:Label>
                      </td>
                    </tr>
                  <tr>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;" class="auto-style4"><b>Regular Holiday OT %</b></td>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;">
                        <asp:Label ID="RegularHolidayOT" runat="server"></asp:Label>
                      </td>
                    </tr>
                  <tr>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;" class="auto-style4"><b>Regular Holiday ND%</b></td>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;">
                        <asp:Label ID="RegularHolidayND" runat="server"></asp:Label>
                      </td>
                    </tr>
                  <tr>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;" class="auto-style4"><b>Special Holiday %</b></td>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;">
                        <asp:Label ID="SpecialHoliday" runat="server"></asp:Label>
                      </td>
                    </tr>
                  <tr>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;" class="auto-style4"><b>Special Holiday OT %</b></td>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;">
                        <asp:Label ID="SpecialHolidayOT" runat="server"></asp:Label>
                      </td>
                    </tr>
                  <tr>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;" class="auto-style4"><b>Special Holiday ND%</b></td>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;">
                        <asp:Label ID="SpecialHolidayND" runat="server"></asp:Label>
                      </td>
                    </tr>
                  <tr>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;" class="auto-style4"><b>Bank Account</b></td>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;">
                        <asp:Label ID="BankAccount" runat="server"></asp:Label>
                      </td>
                    </tr>
                  <tr>
                    <td style="border-style: solid; border-width: thin; font-family: calibri;" class="auto-style4"><b>Tax Status</b></td>
                   <td style="border-style: solid; border-width: thin; font-family: calibri;">
                        <asp:Label ID="TexStatus" runat="server"></asp:Label>
                      </td>
                  </tr>
            </table>
            </div>
        <div >
            <iframe id="pdf" runat="server" style="width: 394px; height: 448px; margin-left: 0px; margin-top: 0px;"  > 
       
        </iframe>
    </div>
        </div>
        <footer style="margin-left: 1200px">
            <asp:Button ID="Button1" runat="server" Text="View Contract" Width="100px" style="margin-left: 0px"/>
            
        </footer>
    </form>
</body>
</html>