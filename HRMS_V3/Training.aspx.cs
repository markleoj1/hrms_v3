﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using HRMS_V3.App_Code;

namespace HRMS_V3
{
    public partial class Training : System.Web.UI.Page
    {
        
        MethodAndVariables train = new MethodAndVariables();
        protected void Page_Load(object sender, EventArgs e)
        {

            traindate.Text = "";
            duedate.Text = "";
        }

        protected void uploadbtn_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                string td = Request.Form[traindate.UniqueID];
                string dd = Request.Form[duedate.UniqueID];
                string filename = Path.GetFileName(topicfile.PostedFile.FileName).Replace(".pdf", "");
                string filepath = "pdf/" + filename + ".pdf";

                topicfile.SaveAs(Server.MapPath("files/" + filename + ".pdf"));
                train.AddTraining(td, dd, filename, filepath);
                topicdrp.DataBind();
            }

        }



        protected void topicdrp_SelectedIndexChanged(object sender, EventArgs e)
        {
            string flname = topicdrp.SelectedValue.ToString();
            string topfile;

            traindate.Text = train.getTrainingDate(flname);
            duedate.Text = train.getTrainingDueDate(flname);
            topfile = train.getTrainingDueTopicFile(flname);
            trainview.Attributes.Add("src", topfile);
        }
    }
}